from math import log10, pow


def create_vocabulary(documents):
  text = []
  for doc in documents:
    text.extend(doc.split(" "))
  return set(text)


def term_frequency(term, document):
  return sum(1 if i == term else 0 for i in document.split(" "))


def term_frequencies(term, documents):
  return [term_frequency(term, doc) for doc in documents]


def calc_tf(documents, vocabulary):
  tf_map = {}
  for word in vocabulary:
    tf_map[word] = term_frequencies(word, documents)
  return tf_map


def calc_df(documents, vocabulary, tf_map):
  df_map = {}
  for word in vocabulary:
    count = 0
    for doc_idx, doc in enumerate(documents):
      count += 1 if tf_map[word][doc_idx] > 0 else 0
    df_map[word] = count
  return df_map


def calc_tf_idf(documents, vocabulary):
  tf_map = calc_tf(documents, vocabulary)
  df_map = calc_df(documents, vocabulary, tf_map)

  N = len(documents)
  tf_idf_vector = []
  for doc_idx, doc in enumerate(documents):
    vector = []
    for word in vocabulary:
      tf = tf_map[word][doc_idx]
      df = df_map[word]
      tf_idf_value = tf * log10(N / df) if df != 0 else 0
      vector.append(tf_idf_value)
    tf_idf_vector.append(vector)
  return tf_idf_vector


def scalar_product(x, y):
  return sum(a * b for a, b in zip(x, y))


def cosine_sim(x, y):
  sp = scalar_product(x, y)
  x_length = pow(sum(i ** 2 for i in x), 1 / len(x))
  y_length = pow(sum(i ** 2 for i in y), 1 / len(y))
  return sp / (x_length * y_length)


def cosine_similarity_matrix(X, y):
  return [cosine_sim(x, y) for x in X]


def bm25_matrix(documents, query: str, df_map, k=1, b=0.5):
  # https: // en.wikipedia.org / wiki / Okapi_BM25
  def bm25(avg_document_length: float, document: str):
    def idf_qi(word):
      df = df_map[word]
      return log10((len(documents) - df + 0.5) / (df + 0.5))

    res = 0
    for word in query.split(" "):
      idf = idf_qi(word)
      tf = term_frequency(word, document)
      bm25_score = idf * (tf * (k + 1)) / (tf + k * ((1 - b) + b * (len(documents) / avg_document_length)))
      res += bm25_score
    return res

  N = len(documents)
  avg_len = sum(len(i.split(" ")) for i in documents) / N
  matrix = [bm25(avg_len, doc) for doc in documents]
  return matrix


def arg_max(lst, documents):
  return documents[max(enumerate(lst), key=lambda x: x[1])[0]]


documents = ["romeo juliet",
             "juliet happy dagger",
             "romeo die dagger",
             "live free die hampshire motto",
             "hampshire england",
             "die dagger"]  # <-- query

vocabulary = create_vocabulary(documents[:-1])
tf_idf_matrix = calc_tf_idf(documents, vocabulary)

query = tf_idf_matrix[-1]
tf_idf_matrix = tf_idf_matrix[:-1]

print(vocabulary)
print()
sim_matrix = cosine_similarity_matrix(tf_idf_matrix, query)
closest_doc = arg_max(sim_matrix, documents)
print(sim_matrix)
print(closest_doc)

print()
print("BM25")


def bm25_wrapper(documents):
  vocabulary = create_vocabulary(documents[:-1])
  bm25 = bm25_matrix(documents[:-1],
                     documents[-1],
                     calc_df(documents[:-1],
                             vocabulary,
                             calc_tf(documents[:-1],
                                     vocabulary)))
  best_fit = arg_max(bm25, documents)
  return bm25, best_fit


bm25, closest_doc = bm25_wrapper(documents)
print(bm25)
print(closest_doc)
print()

documents = ["a b c b d",
             "b e f b",
             "b g c d",
             "b d e",
             "a b e g",
             "b g h h",
             "a c h"]

bm25, closest_doc = bm25_wrapper(documents)
print(bm25)
print(closest_doc)

# for vector, doc in zip(tf_idf_vector, documents):
#   print(doc)
#   for word, tf_idf in zip(vocabulary, vector):
#     print(word, tf_idf, end=", ")
#   print("\n")
